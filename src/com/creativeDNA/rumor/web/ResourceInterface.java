/*******************************************************************************
 * BBC News Reader
 * Released under the BSD License. See README or LICENSE.
 * Copyright (c) 2011, Digital Lizard (Oscar Key, Thomas Boby)
 * All rights reserved.
 ******************************************************************************/
package com.creativeDNA.rumor.web;

import com.creativeDNA.rumor.model.Category;


public interface ResourceInterface {
	
	public void categoriesLoaded(Category[] items); // called when the categories have loaded

}
