package com.creativeDNA.rumor;

import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.creativeDNA.rumor.db.DatabaseHandler;
import com.creativeDNA.rumor.service.ServiceManager;
import com.creativeDNA.rumor.service.ServiceManager.MessageReceiver;


public class MainActivity extends ActionBarActivity 
        implements NavigationDrawerFragment.NavigationDrawerCallbacks,ActionBar.OnNavigationListener,MessageReceiver
        {

	/* variables */
	private DatabaseHandler database;
	private ServiceManager service;
	private SharedPreferences settings;
	private boolean loadInProgress;
	private long lastLoadTime;
	private boolean errorWasFatal;
	private boolean errorDuringThisLoad;
	private boolean firstRun;
	private Dialog errorDialog;
	private Dialog firstRunDialog;
	private Dialog backgroundLoadDialog;
	private Menu menu;
	static final int ACTIVITY_CHOOSE_CATEGORIES = 1;
	private String[] menuItems;
	ActionBar actionBar;
	
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;
    
    /**
     * The serialization (saved instance state) Bundle key representing the
     * current dropdown position.
     */
    private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
   

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    	loadInProgress = false;
		lastLoadTime = 0;
		
		// load the preferences system
		settings = getSharedPreferences(AppConstants.PREFS_FILE_NAME, MODE_PRIVATE);
		loadSettings();
		// load the database, init it if required
				database = new DatabaseHandler(this);
				firstRun = false;
				// bind the service
				service = new ServiceManager(this, this);
				service.doBindService();
				if (!database.isCreated()) {
					// add the categories and set the version to prevent this happening twice
//					database.addCategoriesFromXml();
					// tell the service to load the categories
					service.sendMessageToService(AppConstants.MSG_LOAD_CATEGORIES);
					
					Editor editor = settings.edit();
					editor.putInt(AppConstants.PREFKEY_CATEGORY_UPDATE_VERSION, AppConstants.CURRENT_CATEGORY_UPDATE_VERSION);
					editor.commit();
					
					// proceed with the first run
					firstRun = true;
					
				}
        
				// check if an update is required, if the stored category version is less than the current one
				if(settings.getInt(AppConstants.PREFKEY_CATEGORY_UPDATE_VERSION, AppConstants.DEFAULT_CATEGORY_UPDATE_VERSION) 
						< AppConstants.CURRENT_CATEGORY_UPDATE_VERSION) {
					// run an update
					database.updateCategoriesFromXml();
					
					// set the preference value to the current version
					Editor editor = settings.edit();
					editor.putInt(AppConstants.PREFKEY_CATEGORY_UPDATE_VERSION, AppConstants.CURRENT_CATEGORY_UPDATE_VERSION);
					editor.commit();
				}
				
			
				
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        
        
    }
    void showFirstRunDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		String message = "Choose the categories you are interested in. \n\n"
				+ "The fewer categories enabled the lower data usage and the faster loading will be.";
		builder.setMessage(message);
		builder.setCancelable(false);
		builder.setPositiveButton("Choose", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				closeFirstRunDialog();
				// show the category chooser
				showCategoryChooser();
			}
		});
		firstRunDialog = builder.create();
		firstRunDialog.show();
	}
    void showCategoryChooser() {
		// launch the category chooser activity
		Intent intent = new Intent(this, CategoryChooserActivity.class);
		startActivityForResult(intent, ACTIVITY_CHOOSE_CATEGORIES);
	}
    void closeFirstRunDialog() {
		firstRunDialog = null; // destroy the dialog
	}
    void loadSettings() {
		// check the settings file exists
		if (settings != null) {
			// load values from the settings
			setLastLoadTime(settings.getLong("lastLoadTime", 0)); // sets to zero if not in preferences
		}
	}

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
            case 4:
                mTitle = getString(R.string.title_section4);
                break;
            case 5:
                mTitle = getString(R.string.title_section4);
                break;
        }
    }

    public void restoreActionBar() {
    	 actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.action_bar));
        actionBar.setIcon(getResources().getDrawable(R.drawable.ic_icon));
        actionBar.setDisplayShowTitleEnabled(false);
        
    
        
    }
    
    public void setMenu(){
    	// Set up the dropdown list navigation in the action bar.
        actionBar.setListNavigationCallbacks(
                // Specify a SpinnerAdapter to populate the dropdown list.
                new ArrayAdapter<String>(
                        actionBar.getThemedContext(),
                        android.R.layout.simple_list_item_1,
                        android.R.id.text1,
                        new String[] {
                                getString(R.string.title_section1),
                                getString(R.string.title_section2),
                                getString(R.string.title_section3),
                        }),
                this);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore the previously serialized current dropdown position.
        if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
            getSupportActionBar().setSelectedNavigationItem(
                    savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
        }
    }
    
    @Override
    public void onSaveInstanceState(Bundle outState) {
        // Serialize the current dropdown position.
        outState.putInt(STATE_SELECTED_NAVIGATION_ITEM,
                getSupportActionBar().getSelectedNavigationIndex());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            this.menu = menu; // store a reference for later
            restoreActionBar();
            return true;
        }
        return true; // we have made the menu so we can return true
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }


	/**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
                case 2:
                    return getString(R.string.title_section3).toUpperCase(l);
                case 3:
                    return getString(R.string.title_section4).toUpperCase(l);
            }
            return null;
        }
    }

	@Override
	public boolean onNavigationItemSelected(int position, long id) {
		   // When the given dropdown item is selected, show its contents in the
        // container view.
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
        return true;
	}

	@Override
	public void handleMessage(Message msg) {
		// decide what to do with the message
		switch (msg.what) {
		case AppConstants.MSG_CLIENT_REGISTERED:
			// start a load if we haven't loaded within half an hour
			// TODO make the load time configurable
			long difference = System.currentTimeMillis() - (lastLoadTime * 1000); // the time since the last load
			if (lastLoadTime == 0 || difference > (60 * 60 * 1000)) {
				// don't load if this is the first run
				if(!firstRun) {
					loadData(); // trigger a load
				}
			}
			break;
		case AppConstants.MSG_ERROR:
			Bundle bundle = msg.getData(); // retrieve the data
			errorOccured(bundle.getInt(AppConstants.KEY_ERROR_TYPE), bundle.getString(AppConstants.KEY_ERROR_MESSAGE),
					bundle.getString(AppConstants.KEY_ERROR_ERROR));
			break;
		case AppConstants.MSG_NOW_LOADING:
			loadBegun();
			break;
		case AppConstants.MSG_FULL_LOAD_COMPLETE:
			fullLoadComplete();
			break;
		case AppConstants.MSG_CATEGORIES_LOAD_COMPLETE:
			showFirstRunDialog();
			break;
			
		case AppConstants.MSG_RSS_LOAD_COMPLETE:
			rssLoadComplete();
			break;
		case AppConstants.MSG_UPDATE_LOAD_PROGRESS:
			int totalItems = msg.getData().getInt("totalItems");
			int itemsLoaded = msg.getData().getInt("itemsDownloaded");
			updateLoadProgress(totalItems, itemsLoaded);
			break;
		
			
		default:
			break;
		}
		
		
		
	}
	void loadData() {
		// check we aren't currently loading news
		if (!loadInProgress) {
			// TODO display old news as old
			// tell the service to load the data
			service.sendMessageToService(AppConstants.MSG_LOAD_DATA);
			errorDuringThisLoad = false;
			
//			// hide the refresh button and show the stop button
//			menu.findItem(R.id.menuItemRefresh).setVisible(false);
//			menu.findItem(R.id.menuItemStop).setVisible(true);
		}
	}
	void updateLoadProgress(int totalItems, int itemsLoaded) {
		setStatusText("Preloading " + itemsLoaded + " of " + totalItems + " items");
	}
	
	void rssLoadComplete() {
		// check we are actually loading news
		if (loadInProgress) {
			// tell the user what is going on
			setStatusText("Loading items...");
		}
	}
	void fullLoadComplete() {
		// check we are actually loading news
		if (loadInProgress) {
			loadInProgress = false;
			
			// hide the stop button and show the refresh button
			menu.findItem(R.id.menuItemRefresh).setVisible(true);
			menu.findItem(R.id.menuItemStop).setVisible(false);
			
			// report the loaded status
			setLastLoadTime(settings.getLong("lastLoadTime", 0)); // set the time as unix time
			
			// tell the database to delete old items
			database.clearOld();
		}
	}
	void setLastLoadTime(long time) {
		lastLoadTime = time; // store the time
		// display the new time to the user
		// check if the time is set
		if (!loadInProgress) {
			if (lastLoadTime == 0) {
				// say we have never loaded
				setStatusText("Never updated.");
			}
			else {
				// set the text to show date and time
				String status = "Updated ";
				// find out time since last load in milliseconds
				long difference = System.currentTimeMillis() - (time * 1000); // the time since the last load
				// if within 1 hour, display minutes
				if (difference < (1000 * 60 * 60)) {
					int minutesAgo = (int) Math.floor((difference / 1000) / 60);
					if (minutesAgo == 0) {
						status += "just now";
					}
					else if (minutesAgo == 1) {
						status += minutesAgo + " min ago";
					}
					else {
						status += minutesAgo + " mins ago";
					}
				}
				else {
					// if we are within 24 hours, display hours
					if (difference < (1000 * 60 * 60 * 24)) {
						int hoursAgo = (int) Math.floor(((difference / 1000) / 60) / 60);
						if (hoursAgo == 1) {
							status += hoursAgo + " hour ago";
						}
						else {
							status += hoursAgo + " hours ago";
						}
					}
					else {
						// if we are within 2 days, display yesterday
						if (difference < (1000 * 60 * 60 * 48)) {
							status += "yesterday";
						}
						else {
							// we have not updated recently
							status += "ages ago";
							// TODO more formal message?
						}
					}
				}
				setStatusText(status);
			}
		}
	}
	void errorOccured(int type, String msg, String error) {
		// check if we need to fill in the error messages
		if (msg == null) {
			msg = "null";
		}
		if (error == null) {
			error = "null";
		}
		
		// check if we need to shutdown after displaying the message
		if (type == AppConstants.ERROR_TYPE_FATAL) {
			errorWasFatal = true;
		}
		
		// show a user friendly message or just the error
		if (settings.getBoolean("displayFullError", AppConstants.DEFAULT_DISPLAY_FULL_ERROR)) {
			showErrorDialog("Error: " + error);
		}
		else {
			// display a user friendly message
			if (type == AppConstants.ERROR_TYPE_FATAL) {
				showErrorDialog("Fatal error:\n" + msg + "\nPlease try resetting the app.");
				Log.e("BBC News Reader", "Fatal error: " + msg);
				Log.e("BBC News Reader", error);
			}
			else if (type == AppConstants.ERROR_TYPE_GENERAL) {
				showErrorDialog("Error:\n" + msg);
				Log.e("BBC News Reader", "Error: " + msg);
				Log.e("BBC News Reader", error);
			}
			else if (type == AppConstants.ERROR_TYPE_INTERNET) {
				// only allow one internet error per load
				if (!errorDuringThisLoad) {
					errorDuringThisLoad = true;
					showErrorDialog("Please check your internet connection.");
				}
				Log.e("BBC News Reader", "Error: " + msg);
				Log.e("BBC News Reader", error);
			}
		}
	}
	void showErrorDialog(String error) {
		// only show the error dialog if one isn't already visible
		if (errorDialog == null) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(error);
			builder.setCancelable(false);
			builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					closeErrorDialog();
				}
			});
			errorDialog = builder.create();
			errorDialog.show();
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// wait for activities to send us result data
		switch (requestCode) {
		case ACTIVITY_CHOOSE_CATEGORIES:
			// check the request was a success
			if (resultCode == RESULT_OK) {
				
//				// refresh the display
//				loadData(); // make sure selected categories are loaded
//				FrontpageFragment frontpage = (FrontpageFragment) getSupportFragmentManager().findFragmentById(R.id.frontpageFragment);
//				frontpage.createNewsDisplay(getLayoutInflater(), frontpage.getView()); // reload the ui
//				
//				// check for a first run
//				if (firstRun) {
//					showBackgroundLoadDialog();
//				}
				
				Toast.makeText(MainActivity.this, "Categories Selected", Toast.LENGTH_LONG).show();
			}
			break;
		}
	}
	
	
	void closeErrorDialog() {
		errorDialog = null; // destroy the dialog
		// see if we need to end the program
		if (errorWasFatal) {
			// crash out
			// Log.e("BBC News Reader", "Oops something broke. We'll crash now.");
			System.exit(1); // closes the app with an error code
		}
	}
	void loadBegun() {
		loadInProgress = true; // flag the data as being loaded
		// show the loading image on the button
		// TODO update the load button
		// tell the user what is going on
		setStatusText("Loading feeds...");
	}
	void setStatusText(String text) {
		// set the text to the action bar
		getSupportActionBar().setTitle(text);
	}
}
