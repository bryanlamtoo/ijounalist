package com.creativeDNA.rumor.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.creativeDNA.rumor.R;
import com.creativeDNA.rumor.adapter.CategoryChooserAdapter;
import com.creativeDNA.rumor.db.DatabaseHandler;
import com.mobeta.android.dslv.DragSortListView;

public class CategoryChooserFragment extends Fragment {
	
	/* constants */

	/* variables */
	private DatabaseHandler database;
	private CategoryChooserAdapter adapter;
	private DragSortListView listView;
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// load the categories from the database
		database = new DatabaseHandler(getActivity());
		String[] enabledCategories = database.getEnabledCategories()[1];
		String[] disabledCategories = database.getDisabledCategories()[1];
		
		System.out.println("DISABLED CATEGORIES "+disabledCategories.length);
		
		// create the list adapter
		adapter = new CategoryChooserAdapter(getActivity(), 
				enabledCategories, disabledCategories);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// inflate the layout
		View view = inflater.inflate(R.layout.category_chooser, container, false);
		
		// connect the listview to the adapter and set dragging stuff
		listView = (DragSortListView) view.findViewById(R.id.categoryChooserListView);
		listView.setAdapter(adapter);
		listView.setDropListener(dropListener);
				
		return view;
	}
	
	private DragSortListView.DropListener dropListener = new DragSortListView.DropListener() {
		@Override
		public void drop(int from, int to) {
			// remove the item from the old position and insert it at the new one
			if(from != to) {
				String item = adapter.getItem(from);
				adapter.move(item, from, to);
			}
		}
	};
	
	public void saveCategories() {
		database.setCategoryStates(adapter.getEnabledCategories(), adapter.getDisabledCategories());
	}
}
