package com.creativeDNA.rumor.adapter;


import java.util.ArrayList;

import com.creativeDNA.rumor.R;
import com.creativeDNA.rumor.model.NavigationItem;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class NavigationDrawerAdapter extends ArrayAdapter<NavigationItem>{
	
	private Context mContext;
	private int mLayout;
	private ViewHolder mViewHolder;
	private LayoutInflater mInflater;
	private Typeface font;
	private ArrayList<NavigationItem> navigationItems;

	public NavigationDrawerAdapter(Context context, int resource,ArrayList<NavigationItem> navigationItems) {
		super(context, resource);
		mContext = context;
		mLayout = resource;
		mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		font = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
		this.navigationItems = navigationItems;
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Toast.makeText(mContext, "REached here ", Toast.LENGTH_LONG).show();
		if(convertView == null){
			//initialise the view
			convertView = mInflater.inflate(mLayout, null);
			mViewHolder = new ViewHolder();
			mViewHolder.mTitle = (TextView) convertView.findViewById(R.id.iconName);
			mViewHolder.mIcon = (ImageView) convertView.findViewById(R.id.icon);
			
			
			convertView.setTag(mViewHolder);
		}else{
			mViewHolder = (ViewHolder) convertView.getTag();
			
		}
		
		//set the necessary values
		mViewHolder.mTitle.setTypeface(font);
		mViewHolder.mTitle.setText(navigationItems.get(position).getmTitle());
		mViewHolder.mIcon.setImageResource(navigationItems.get(position).getmIcon());
		
		Toast.makeText(mContext, "Title "+navigationItems.get(position).getmTitle(), Toast.LENGTH_LONG).show();
		
		return convertView;
	}
	
	static class ViewHolder {
		TextView mTitle;
		ImageView mIcon;
		}

}
