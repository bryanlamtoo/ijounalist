package com.creativeDNA.rumor.db;

import java.util.Date;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;

import com.creativeDNA.rumor.AppConstants;
import com.creativeDNA.rumor.R;



public class DatabaseHandler {

	private Context context;
	private ContentResolver contentResolver;
	private long clearOutAgeMilliSecs;
	private ItemClearer itemClearer;
	
	
	public DatabaseHandler() {
		// TODO Auto-generated constructor stub
	}

	public DatabaseHandler(Context context) {
		this.context = context;
		this.contentResolver = context.getContentResolver();
		
		SharedPreferences settings = context.getSharedPreferences(AppConstants.PREFS_FILE_NAME, Context.MODE_PRIVATE);
		clearOutAgeMilliSecs = settings.getInt("clearOutAge", AppConstants.DEFAULT_CLEAR_OUT_AGE) * 24 * 60 * 60 * 1000;
		
		itemClearer = new ItemClearer();
	}
	/**
	 * Checks whether there are any records in category
	 * 
	 * @return true or false
	 */
	public boolean isCreated() {
		try {
			getCategoryBooleans()[0] = true;
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	/**
	 * Queries the categories table for the enabled column of all rows, returning an array of booleans representing whether categories are enabled or
	 * not, sorted by category_Id.
	 * 
	 * @return boolean[] containing enabled column from categories table.
	 */
	public boolean[] getCategoryBooleans() {
		Uri uri = DatabaseProvider.CONTENT_URI_CATEGORIES;
		String[] projection = new String[] { DatabaseHelper.COLUMN_CATEGORY_ENABLED };
		Cursor cursor = contentResolver.query(uri, projection, null, null, DatabaseHelper.COLUMN_CATEGORY_ID);
		boolean[] enabledCategories = new boolean[cursor.getCount()];
		while (cursor.moveToNext()) {
			if (cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_CATEGORY_ENABLED)) == 0) {
				enabledCategories[cursor.getPosition()] = false;
			}
			else {
				enabledCategories[cursor.getPosition()] = true;
			}
		}
		cursor.close();
		return enabledCategories;
	}
	/**
	 * Adds all the start categories from the XML
	 */
	public void addCategoriesFromXml() {
		try {
			String[] categoryNames = context.getResources().getStringArray(R.array.category_names);
			String[] categoryUrls = context.getResources().getStringArray(R.array.catergory_rss_urls);
			int[] categoryBooleans = context.getResources().getIntArray(R.array.category_default_booleans);
			for (int i = 0; i < categoryNames.length; i++) {
				boolean enabled = true;
				if (categoryBooleans[i] == 0) {
					enabled = false;
				}
				insertCategory(categoryNames[i], enabled, categoryUrls[i]);
			}
		} catch (NullPointerException e) {
			// Log.e("Database", "Categories XML is broken.");
		}
	}
	
	public void updateCategoriesFromXml() {
		// FIXME this is not a reliable way to update categories because it has the potential to disrupt the order
		// which is required to be the same as xml (really? might no longer be the case)
		try {
			// get all the categories from the xml and database and check for any mismatches
			String[] xmlCategoryNames = context.getResources().getStringArray(R.array.category_names);
			String[] xmlCategoryUrls = context.getResources().getStringArray(R.array.catergory_rss_urls);
			int[] xmlCategoryBooleans = context.getResources().getIntArray(R.array.category_default_booleans);
			
			String[][] databaseCategories = getAllCategories();
			// check that some categories were returned
			if(databaseCategories[0].length == 0) {
				// bail here
				return;
			}
			
			// if both the xml and the database have the same number of categories in them, we don't need to do anything
			if(databaseCategories[0].length == xmlCategoryNames.length) {
				return;
			}
			
			// check if each category from the xml is present in the database
			for(int i = 0; i < xmlCategoryNames.length; i++) {
				boolean present = false;
				for(int j = 0; j < databaseCategories[1].length; j++) {
					if(xmlCategoryNames[i].equals(databaseCategories[1][j])) {
						present = true;
					}
				}
				
				if(present == false) {
					// if the category isn't present, add it to the end of the table
					boolean enabled = true;
					if (xmlCategoryBooleans[i] == 0) {
						enabled = false;
					}
					insertCategory(xmlCategoryNames[i], enabled, xmlCategoryUrls[i]);
				}
			}
			
		} catch (NullPointerException e) {
			// don't log the error
		}
	}
	/**
	 * Inserts a category into the category table.
	 * 
	 * @param name
	 *            Name of the category as String
	 * @param enabled
	 *            Whether the RSSFeed should be fetched as Boolean
	 */
	public void insertCategory(String name, boolean enabled, String url) {
		Uri uri = DatabaseProvider.CONTENT_URI_CATEGORIES;
		int enabledInt = (enabled) ? 1 : 0;
		ContentValues values = new ContentValues(3);
		values.put(DatabaseHelper.COLUMN_CATEGORY_NAME, name);
		values.put(DatabaseHelper.COLUMN_CATEGORY_ENABLED, enabledInt);
		values.put(DatabaseHelper.COLUMN_CATEGORY_URL, url);
		contentResolver.insert(uri, values);
	}
	/**
	 * Returns the links and names of all the categories
	 * 
	 * @return A string[][] containing the String urls in [0] and String names in [1].
	 */
	public String[][] getAllCategories() {
		// query the DatabaseProvider for the categories
		Uri uri = DatabaseProvider.CONTENT_URI_CATEGORIES; // uri for all the categories
		String[] projection = new String[] { DatabaseHelper.COLUMN_CATEGORY_URL, DatabaseHelper.COLUMN_CATEGORY_NAME };
		Cursor cursor = contentResolver.query(uri, projection, null, null, DatabaseHelper.COLUMN_CATEGORY_ID);
		
		// check if no rows were returned
		if (cursor == null) {
			// bail here, returning an empty 2d array
			return new String[2][0];
		}
		
		// find the column indexes
		int url = cursor.getColumnIndex(DatabaseHelper.COLUMN_CATEGORY_URL);
		int name = cursor.getColumnIndex(DatabaseHelper.COLUMN_CATEGORY_NAME);
		
		// loop through and save these categories to an array
		String[][] categories = new String[2][cursor.getCount()];
		while (cursor.moveToNext()) {
			categories[0][cursor.getPosition()] = cursor.getString(url);
			categories[1][cursor.getPosition()] = cursor.getString(name);
		}
		cursor.close();
		
		return categories;
	}
	/**
	 * Returns the links and names of all the categories that are enabled.
	 * 
	 * @return A string[][] containing the String urls in [0] and String names in [1].
	 */
	public String[][] getEnabledCategories() {
		// query the DatabaseProvider for the categories
		Uri uri = DatabaseProvider.CONTENT_URI_ENABLED_CATEGORIES; // uri for enabled categories
		String[] projection = new String[] { DatabaseHelper.COLUMN_CATEGORY_URL, DatabaseHelper.COLUMN_CATEGORY_NAME };
		String sortOrder = DatabaseHelper.COLUMN_CATEGORY_PRIORITY + ", " + DatabaseHelper.COLUMN_CATEGORY_ID;
		Cursor cursor = contentResolver.query(uri, projection, null, null, sortOrder);
		
		// check if no rows were returned
		if (cursor == null) {
			// bail here, returning an empty 2d array
			return new String[2][0];
		}
		
		// find the column indexes
		int url = cursor.getColumnIndex(DatabaseHelper.COLUMN_CATEGORY_URL);
		int name = cursor.getColumnIndex(DatabaseHelper.COLUMN_CATEGORY_NAME);
		
		// loop through and save these categories to an array
		String[][] categories = new String[2][cursor.getCount()];
		while (cursor.moveToNext()) {
			categories[0][cursor.getPosition()] = cursor.getString(url);
			categories[1][cursor.getPosition()] = cursor.getString(name);
		}
		cursor.close();
		
		return categories;
	}
	/**
	 * When called will remove all articles that are over the threshold, to the second, old. Then cleans up the relationship table. Possibly resource
	 * intensive.
	 */
	public void clearOld() {
		// delete items older than the threshold
		Date now = new Date();
		SharedPreferences settings = context.getSharedPreferences(AppConstants.PREFS_FILE_NAME, Context.MODE_PRIVATE);
		clearOutAgeMilliSecs = settings.getInt("clearOutAge", AppConstants.DEFAULT_CLEAR_OUT_AGE) * 24 * 60 * 60 * 1000;
		long threshold = (now.getTime() - clearOutAgeMilliSecs);
		
		itemClearer.clearItems(contentResolver, threshold);
	}
	/**
	 * Returns the links and names of all the categories that are disabled.
	 * 
	 * @return A string[][] containing the String urls in [0] and String names in [1].
	 */
	public String[][] getDisabledCategories() {
		// query the DatabaseProvider for the categories
		Uri uri = DatabaseProvider.CONTENT_URI_DISABLED_CATEGORIES;
		String[] projection = new String[] { DatabaseHelper.COLUMN_CATEGORY_URL, DatabaseHelper.COLUMN_CATEGORY_NAME };
		String sortOrder = DatabaseHelper.COLUMN_CATEGORY_ID;
		Cursor cursor = contentResolver.query(uri, projection, null, null, sortOrder);
		
		// check if no rows were returned
		if (cursor == null) {
			// bail here, returning an empty 2d array
			return new String[2][0];
		}
		
		// find the column indexes
		int url = cursor.getColumnIndex(DatabaseHelper.COLUMN_CATEGORY_URL);
		int name = cursor.getColumnIndex(DatabaseHelper.COLUMN_CATEGORY_NAME);
		
		// loop through and save these categories to an array
		String[][] categories = new String[2][cursor.getCount()];
		while (cursor.moveToNext()) {
			categories[0][cursor.getPosition()] = cursor.getString(url);
			categories[1][cursor.getPosition()] = cursor.getString(name);
		}
		cursor.close();
		
		return categories;
	}
	public void setCategoryStates(String[] enabledCategories, String[] disabledCategories) {
		// loop through setting the state and priority of enabled categories
		ContentValues values = new ContentValues(2);
		for (int i = 0; i < enabledCategories.length; i++) {
			Uri uri = Uri.withAppendedPath(DatabaseProvider.CONTENT_URI_CATEGORY_BY_NAME, 
					enabledCategories[i]);
			
			values.clear();
			values.put(DatabaseHelper.COLUMN_CATEGORY_ENABLED, 1);
			values.put(DatabaseHelper.COLUMN_CATEGORY_PRIORITY, i);
			
			contentResolver.update(uri, values, null, null);
		}
		
		
		// set the disabled categories
		StringBuilder where = new StringBuilder();
		String[] selectionArgs = disabledCategories;
		for(int i = 0; i < disabledCategories.length; i++) {
			where.append(DatabaseHelper.COLUMN_CATEGORY_NAME + "=?");
			// don't add an or if this is the final argument
			if(i != disabledCategories.length - 1) {
				where.append(" OR ");
			}
		}
		
		values.clear();
		values.put(DatabaseHelper.COLUMN_CATEGORY_ENABLED, 0);
		
		contentResolver.update(DatabaseProvider.CONTENT_URI_CATEGORIES, values, 
				where.toString(), selectionArgs);
	}
}
