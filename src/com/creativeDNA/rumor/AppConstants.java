package com.creativeDNA.rumor;

import java.util.ArrayList;

public class AppConstants {

	public static final String PREFKEY_LOAD_IN_BACKGROUND = "loadInBackground";
	public static final String PREFKEY_RTC_WAKEUP = "rtcWakeup";
	public static final String PREFKEY_LOAD_INTERVAL = "loadInterval";
	public static final String PREFKEY_CATEGORY_UPDATE_VERSION = "categoryUpdateVersion";
	public static final String ACTION_LOAD = "com.creativeDNA.rumor.action.LOAD";
	public static final String PREFS_FILE_NAME = "com.creativeDNA.rumor_preference";
	public static final int DEFAULT_CLEAR_OUT_AGE = 4;
	public static final boolean DEFAULT_LOAD_IN_BACKGROUND = true;
	public static final boolean DEFAULT_RTC_WAKEUP = true;
	public static final String DEFAULT_LOAD_INTERVAL = "1_hour";
	public static final boolean DEFAULT_DISPLAY_FULL_ERROR = false;
	public static final int ERROR_TYPE_GENERAL = 0;
	public static final int ERROR_TYPE_INTERNET = 1;
	public static final int ERROR_TYPE_FATAL = 2;
	public static final String KEY_ERROR_TYPE = "type";
	public static final String KEY_ERROR_MESSAGE = "message";
	public static final String KEY_ERROR_ERROR = "error";
	public static final String KEY_CATEGORY = "category";
	public static final int MSG_REGISTER_CLIENT = 1;
	public static final int MSG_UNREGISTER_CLIENT = 2;
	public static final int MSG_CLIENT_REGISTERED = 3; // returned to a client when registered
	public static final int MSG_LOAD_DATA = 4; // sent to request a data load
	public static final int MSG_LOAD_ARTICLE = 11;
	public static final int MSG_LOAD_THUMB = 12;
	public static final int MSG_LOAD_IMAGE = 13;
	public static final int MSG_STOP_DATA_LOAD = 9; // sent to stop data loading
	public static final int MSG_CATEGORY_LOADED = 6; // sent when a category has loaded
	public static final int MSG_ARTICLE_LOADED = 15; // article loaded
	public static final int MSG_THUMB_LOADED = 14; // thumbnail loaded
	public static final int MSG_NOW_LOADING = 16;
	public static final int MSG_FULL_LOAD_COMPLETE = 8; // sent when all the data has been loaded
	public static final int MSG_RSS_LOAD_COMPLETE = 10;
	public static final int MSG_UPDATE_LOAD_PROGRESS = 18;
	public static final int MSG_ERROR = 7; // help! An error occurred
	public static final int DEFAULT_CATEGORY_UPDATE_VERSION = 0;
	public static final int CURRENT_CATEGORY_UPDATE_VERSION = 0;
	public static final int MSG_LOAD_CATEGORIES = 17; // returned to a client when registered
	public static final int MSG_CATEGORIES_LOAD_COMPLETE = 20;
	
	/**
	 * URL of the Backend server
	 */
	public static final String BACKEND_SERVER_URL = "http://192.168.137.1/rare-tape/";
	public static final ArrayList<String> FORBIDDEN_CATEGORY_TITLES = new ArrayList<String>();

}
