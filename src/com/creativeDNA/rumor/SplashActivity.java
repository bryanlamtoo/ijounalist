package com.creativeDNA.rumor;


import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashActivity extends ActionBarActivity implements AnimationListener{
	private Animation mLogoAnimation,mFadeAnimation;
	private ImageView logo;
	private TextView skipLogin;
	private View SignIn;
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		//load the animations
		mLogoAnimation = AnimationUtils.loadAnimation(this, R.anim.logo_down_from_up);
		mFadeAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_in);
		logo = (ImageView) findViewById(R.id.logo);
		SignIn = findViewById(R.id.signInView);
		skipLogin = (TextView) findViewById(R.id.skip_login_button);
		mLogoAnimation.setAnimationListener(this);
		mFadeAnimation.setAnimationListener(this);
		logo.startAnimation(mLogoAnimation);
		SignIn.startAnimation(mFadeAnimation);
		
		
		skipLogin.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 Intent i = new Intent(SplashActivity.this, MainActivity.class);
				 startActivity(i);
			}
		});
		
		Thread timer = new Thread(){
			@Override
			public void run(){
				try{
					sleep(5000);
				 }catch(InterruptedException e){
					 e.printStackTrace();
					 
				 }finally{
					 Intent i = new Intent(SplashActivity.this, MainActivity.class);
					 startActivity(i);
					// SplashActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
					finish(); 
						 
				 }
				
				}
			
			};
		//	timer.start();
	}

	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		
	}



}
