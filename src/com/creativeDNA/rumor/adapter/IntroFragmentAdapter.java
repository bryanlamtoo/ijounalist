package com.creativeDNA.rumor.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.creativeDNA.rumor.R;
import com.creativeDNA.rumor.fragments.IntroFragment;
import com.viewpagerindicator.IconPagerAdapter;

public class IntroFragmentAdapter extends FragmentPagerAdapter implements IconPagerAdapter {
	private int mCount = 3;

    public IntroFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
    	 return IntroFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return mCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
      return "";
    }

   
    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }

	@Override
	public int getIconResId(int index) {
		// TODO Auto-generated method stub
		return 0;
	}
}