package com.creativeDNA.rumor;

import android.annotation.SuppressLint;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.creativeDNA.rumor.adapter.IntroFragmentAdapter;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

public class IntroActivity extends FragmentActivity {

	ViewPager mPager;
    PageIndicator mIndicator;
    IntroFragmentAdapter mAdapter;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_intro);

        mAdapter = new IntroFragmentAdapter(getSupportFragmentManager());

        mPager = (ViewPager)findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);

        mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        mIndicator.setViewPager(mPager);
       
        
        //We set this on the indicator, NOT the pager
        mIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
            	
            	
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        
        mPager.setPageTransformer(false, new ViewPager.PageTransformer() {
			
			@SuppressLint("NewApi") @Override
			public void transformPage(View page, float position) {
				page.setTranslationX(page.getWidth() * -position);

		            if(position <= -1.0F || position >= 1.0F) {
		            	page.setAlpha(0.0F);
		            } else if( position == 0.0F ) {
		            	page.setAlpha(1.0F);
		            } else { 
		                // position is between -1.0F & 0.0F OR 0.0F & 1.0F
		            	page.setAlpha(1.0F - Math.abs(position));
		            }
		        
				
			}
		});

    
	}

	
}
