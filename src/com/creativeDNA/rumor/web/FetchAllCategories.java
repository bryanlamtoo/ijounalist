package com.creativeDNA.rumor.web;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import static com.creativeDNA.rumor.AppConstants.BACKEND_SERVER_URL;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;

import android.os.AsyncTask;

import com.creativeDNA.rumor.AppConstants;
import com.creativeDNA.rumor.MainActivity;
import com.creativeDNA.rumor.db.DatabaseHandler;
import com.creativeDNA.rumor.model.Category;
import com.creativeDNA.rumor.util.CategoryIndexComparator;
import com.creativeDNA.rumor.util.JsonUtil;

/**
 * This class is used to fetch all Categories from the backend server.
 * The operation is done in <code>AsyncTask</code>
 * 
 * @author |the creativeDNA|
 *
 */
public class FetchAllCategories extends
		AsyncTask<Void, Void, ArrayList<Category>> {

	/**
	 * Constant with URL to fetch all categories
	 */
	public static String BACKEND_FETCH_ALL_CATEGORIES_URL = "%scategories.json";

	/**
	 * The <code>Activity</code> that is going to handle the fetched categories.
	 */
	private CategoryManager mResponceActivity;
	
	


	/**
	 * Class constructor
	 * 
	 * @param responceActivity
	 *            The <code>Activity</code> that is going to handle the fetched
	 *            categories.
	 */
	public FetchAllCategories(CategoryManager responceActivity) {
		
		this.mResponceActivity = responceActivity;
	}

	/**
	 * Override this method to perform a computation on a background thread. The
	 * specified parameters are the parameters passed to execute(Params...) by
	 * the caller of this task. This method can call
	 * publishProgress(Progress...) to publish updates on the UI thread.
	 * 
	 * @param result
	 *            The result of the operation computed by
	 *            doInBackground(Params...).
	 * 
	 */
	@Override
	protected ArrayList<Category> doInBackground(Void... params) {

		ArrayList<Category> allCategories = null;

		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response = httpclient.execute(new HttpGet(
					BACKEND_FETCH_ALL_CATEGORIES_URL));
			StatusLine statusLine = response.getStatusLine();
			if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				out.close();
				String responseString = out.toString();

				JSONArray items = JsonUtil.parseJson(responseString)
						.getJSONArray("categories");

				allCategories = new ArrayList<Category>();

				for (int i = 0; i < items.length(); i++) {
					Category category = new Category(items.getJSONObject(i));
					if (!isForbidden(category)) {
						allCategories.add(category);
					}
				}
			} else {
				// Closes the connection.
				response.getEntity().getContent().close();
				throw new IOException(statusLine.getReasonPhrase());

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return allCategories;
	}

	private boolean isForbidden(Category category) {
		boolean result = false;
		if (AppConstants.FORBIDDEN_CATEGORY_TITLES.contains(category.getTitle())) {
			result = true;
		}
		return result;
	}

	/**
	 * Runs on the UI thread after doInBackground(Params...). The specified
	 * result is the value returned by doInBackground(Params...).
	 * 
	 * This method won't be invoked if the task was cancelled.
	 * 
	 * @param result
	 *            The result of the operation computed by
	 *            doInBackground(Params...).
	 */
	@Override
	protected void onPostExecute(ArrayList<Category> result) {
		super.onPostExecute(result);

		if(result != null && result.size() > 0){
			Collections.sort(result, new CategoryIndexComparator());
			
			mResponceActivity.handleData(result);
			
		}
	}

	/**
	 * Runs on the UI thread before doInBackground(Params...).
	 */
	@Override
	protected void onPreExecute() {
		super.onPreExecute();

		
		BACKEND_FETCH_ALL_CATEGORIES_URL = String.format(
				BACKEND_FETCH_ALL_CATEGORIES_URL, BACKEND_SERVER_URL);
		
		System.out.println(BACKEND_FETCH_ALL_CATEGORIES_URL);
	}
}