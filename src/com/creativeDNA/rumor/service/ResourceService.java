package com.creativeDNA.rumor.service;



import java.util.ArrayList;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.creativeDNA.rumor.AppConstants;
import com.creativeDNA.rumor.db.DatabaseHandler;
import com.creativeDNA.rumor.model.Category;
import com.creativeDNA.rumor.web.CategoryManager;
import com.creativeDNA.rumor.web.ResourceInterface;

public class ResourceService extends Service implements ResourceInterface {
	/* variables */
	public boolean loadInProgress; // a flag to tell the activity if there is a load in progress
	SharedPreferences settings;
	DatabaseHandler database; // the database
	BroadcastReceiver broadcastReceiver;
	OnSharedPreferenceChangeListener settingsChangedListener;
	CategoryManager categoryManager;
	
	
	final Messenger messenger = new Messenger(new IncomingHandler()); // the messenger used for communication
	ArrayList<Messenger> clients = new ArrayList<Messenger>(); // holds references to all of our clients
	
	/* command definitions */
	public static final int MSG_REGISTER_CLIENT = 1;
	public static final int MSG_UNREGISTER_CLIENT = 2;
	public static final int MSG_CLIENT_REGISTERED = 3; // returned to a client when registered
	
	
	
	public ResourceService() {
		// TODO Auto-generated constructor stub
	}
	
	class IncomingHandler extends Handler{
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			
			switch(msg.what){
			case MSG_REGISTER_CLIENT:
				clients.add(msg.replyTo); // add a reference to the client to our list
				sendMsg(msg.replyTo, MSG_CLIENT_REGISTERED, null);
				break;
			case MSG_UNREGISTER_CLIENT:
				unregisterClient(msg.replyTo);
				break;
			case AppConstants.MSG_LOAD_CATEGORIES:
				loadCategories(); // start of the loading of data
				break;
			}
		}
	}
	
	void sendMsg(Messenger client, int what, Bundle bundle) {
		try {
			// create a message according to parameters
			Message msg = Message.obtain(null, what);
			if (bundle != null) {
				msg.setData(bundle);
			}
			client.send(msg); // send the message
		} catch (RemoteException e) {
			// We are probably shutting down, but report it anyway
			Log.e("ERROR", "Unable to send message to client: " + e.getMessage());
		}
	}
	private void unregisterClient(Messenger client) {
		// remove our reference to the client
		clients.remove(client);
		// if we have no more clients and a load is not in progress, shutdown
		if(clients.isEmpty() && !loadInProgress) {
			stopSelf();
		}
	}
	boolean isOnline() {
		ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = manager.getActiveNetworkInfo();
		// check that there is an active network
		if (info != null) {
			return info.isConnected();
		}
		else {
			return false;
		}
	}
	@Override
	public void onCreate() {
		
		// init variables
				loadInProgress = false;
				
				// load various key components
				if (settings == null) {
					// load in the settings
					settings = getSharedPreferences(AppConstants.PREFS_FILE_NAME, MODE_PRIVATE); // load settings in read/write form
				}
				if (database == null) {
					// load the database
					setDatabase(new DatabaseHandler(this));
					// create tables in the database if needed
					if (!getDatabase().isCreated()) {
						sendMsgToAll(AppConstants.MSG_LOAD_CATEGORIES, null);
						
					}
				}
				if (categoryManager == null) {
					// load the rss manager
					categoryManager = new CategoryManager(this);
				}
				
				// register to receive alerts when a load is required
				broadcastReceiver = new BroadcastReceiver() {
					@Override
					public void onReceive(Context context, Intent intent) {
						if (intent.getAction().equals("com.creativeDNA.rumor.action.LOAD")) {
							if(database.isCreated()){
									// start a load
									loadData();
								
								}else{
									Log.e("SERVICE", "No categories in db");
									sendMsgToAll(AppConstants.MSG_LOAD_CATEGORIES, null);
								}
						}
					}
				};
				this.registerReceiver(broadcastReceiver, new IntentFilter(AppConstants.ACTION_LOAD));
				
				// load in the settings
				updateSettings();
				
				// register a change listener on the settings
				settingsChangedListener = new OnSharedPreferenceChangeListener() {
					public void onSharedPreferenceChanged(SharedPreferences preferences, String key) {
						// update the settings
						updateSettings();
					}
				};
				settings.registerOnSharedPreferenceChangeListener(settingsChangedListener);
	}
	@Override
	public void onDestroy() {
		// unregister receivers
		this.unregisterReceiver(broadcastReceiver);
		if (settings != null && settingsChangedListener != null) {
			settings.unregisterOnSharedPreferenceChangeListener(settingsChangedListener);
		}
		super.onDestroy();
	}
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// check if we have been told to load news on start
		if (intent != null) {
			if(database.isCreated()){
			if (intent.getAction().equals(AppConstants.ACTION_LOAD)) {
				// start a load
				loadData();
			}
			}else{
				sendMsgToAll(AppConstants.MSG_LOAD_CATEGORIES, null);
			}
		}
		
		// we want to continue running until explicitly stopped, so return sticky.
		return START_STICKY;
	}
	void stopDataLoad() {
		// stop the data loading
		categoryManager.stopLoading();
//		getWebManager().stopDownload();
		// the stopping of loading will be reported by the managers...
	}
	void updateSettings() {
		// get the alarm manager to allow triggering of loads in the future
		AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
		
		// produce the intent to trigger a load
		Intent intent = new Intent(AppConstants.ACTION_LOAD);
		PendingIntent pendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		// PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		// register alarms for background loading
		if (settings.getBoolean(AppConstants.PREFKEY_LOAD_IN_BACKGROUND, AppConstants.DEFAULT_LOAD_IN_BACKGROUND)) {
			// background loading is switched on, register an alarm to trigger loads, first work out the interval
			String loadIntervalString = settings.getString(AppConstants.PREFKEY_LOAD_INTERVAL, AppConstants.DEFAULT_LOAD_INTERVAL);
			long loadInterval;
			if (loadIntervalString.equals("15_mins")) {
				loadInterval = AlarmManager.INTERVAL_FIFTEEN_MINUTES;
			}
			else if (loadIntervalString.equals("30_mins")) {
				loadInterval = AlarmManager.INTERVAL_HALF_HOUR;
			}
			else if (loadIntervalString.equals("1_hour")) {
				loadInterval = AlarmManager.INTERVAL_HOUR;
			}
			else if (loadIntervalString.equals("half_day")) {
				loadInterval = AlarmManager.INTERVAL_HALF_DAY;
			}
			else {
				loadInterval = AlarmManager.INTERVAL_HOUR;
			}
			
			// work out the starting time of the alarm
			long startingTime = System.currentTimeMillis() + loadInterval; // now plus the interval
			
			// register an alarm to start loads, depending on rtc wakeup
			if (settings.getBoolean(AppConstants.PREFKEY_RTC_WAKEUP, AppConstants.DEFAULT_RTC_WAKEUP)) {
				alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, startingTime, loadInterval, pendingIntent);
			}
			else {
				alarmManager.setInexactRepeating(AlarmManager.RTC, startingTime, loadInterval, pendingIntent);
			}
		}
		else {
			// background loading is switched off, cancel alarms
			alarmManager.cancel(pendingIntent);
		}
	}
	void loadData() {
		// check if the device is online
		if (isOnline()) {
			// report to the gui that a load has been activated
			sendMsgToAll(AppConstants.MSG_NOW_LOADING, null);
			// set the flag saying that we are loading
			loadInProgress = true;
			// retrieve the active category urls
			String[][] enabledCategories = getDatabase().getEnabledCategories();
			String[] urls = enabledCategories[0];
			String[] names = enabledCategories[1];
			// start the RSS Manager
		//	rssManager.load(names, urls);
		}
		else {
			// report that there is no internet connection
			reportError(AppConstants.ERROR_TYPE_INTERNET, "There is no internet connection.", null);
		}
	}
	void loadCategories(){
		Log.e("SERVICE", "Loading Categories");
		// check if the device is online
				if (isOnline()) {
					// report to the gui that a load has been activated
					sendMsgToAll(AppConstants.MSG_NOW_LOADING, null);
					// set the flag saying that we are loading
					loadInProgress = true;
					categoryManager.Fetch();
				}else{
					Log.e("OFFLINE", "Please connect to the internet");
				}
	}
	
	
	public synchronized void reportError(int type, String msg, String error) {
		// an error has occurred, send a message to the gui
		// this will display something useful to the user
		Bundle bundle = new Bundle();
		bundle.putInt(AppConstants.KEY_ERROR_TYPE, type);
		bundle.putString(AppConstants.KEY_ERROR_MESSAGE, msg);
		bundle.putString(AppConstants.KEY_ERROR_ERROR, error);
		sendMsgToAll(AppConstants.MSG_ERROR, bundle);
	}
	void sendMsgToAll(int what, Bundle bundle) {
		// loop through and send the message to all the clients
		for (int i = 0; i < clients.size(); i++) {
			sendMsg(i, what, bundle);
		}
	}
	void sendMsg(int clientId, int what, Bundle bundle) {
		// simply call the main sendMessage but with an actual client
		sendMsg(clients.get(clientId), what, bundle);
	}
	public synchronized void setDatabase(DatabaseHandler db) {
		this.database = db;
	}
	public synchronized DatabaseHandler getDatabase() {
		return database;
	}
	

	@Override
	public IBinder onBind(Intent intent) {
		return messenger.getBinder();
	}
	@Override
	public void categoriesLoaded(Category[] items) {
		Log.i("CATEGORIES", "Categories downloaded "+items.length);
		
		for(int i=0;i<items.length;i++){
			getDatabase().insertCategory(items[i].getTitle(), false, items[i].getSlug());
		}
		
		sendMsgToAll(AppConstants.MSG_CATEGORIES_LOAD_COMPLETE, null);
	}

}
